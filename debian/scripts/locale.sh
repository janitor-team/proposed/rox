#!/bin/sh

for d in ROX-Filer/Messages/*  
do
	if [ -d "$d" ]; then
		l=`basename $d`
		mkdir -p debian/rox-filer/usr/share/locale/$l/LC_MESSAGES; 
		cp $d/LC_MESSAGES/ROX-Filer.mo debian/rox-filer/usr/share/locale/$l/LC_MESSAGES/rox.mo; 
	fi
done
